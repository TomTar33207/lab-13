package pl.edu.pwsztar

import spock.lang.Specification

class AddProductSpec extends Specification {

    static ShoppingCart cart;

    def setupSpec() {
        cart = new ShoppingCart();
    }

    def "should add products to shopping cart"() {

        expect:
        cart.addProducts("p1", 5, 1)
        cart.addProducts("p1", 5, 2)
        !cart.addProducts("p1", 6, 1)
        !cart.addProducts("p2", 10, -1)
        !cart.addProducts("p3", -10, 1)
        cart.addProducts("p3", 10, 1)
        !cart.addProducts("p3", 10, 500)
    }

}
