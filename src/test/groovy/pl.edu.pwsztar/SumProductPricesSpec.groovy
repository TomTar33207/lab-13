package pl.edu.pwsztar

import spock.lang.Specification

class SumProductPricesSpec extends Specification {

    static ShoppingCart cart;

    def setupSpec() {
        cart = new ShoppingCart();
    }

    def "should get sum price of products from shopping cart"() {

        when:
        cart.addProducts("p1", 1, 10)
        cart.addProducts("p2", 2, 5)

        then:
        cart.getSumProductsPrices() == 20
        cart.deleteProducts("p1", 5)
        cart.getSumProductsPrices() == 15
        cart.addProducts("p2", 2, 10)
        cart.getSumProductsPrices() == 35

        cart.deleteProducts("p1", 5)
        cart.deleteProducts("p2", 15)
        cart.getProductPrice() == 0
    }

}
