package pl.edu.pwsztar

import spock.lang.Specification

class DeleteProductSpec extends Specification {

    static ShoppingCart cart;

    def setupSpec() {
        cart = new ShoppingCart();
    }

    def "should delete products from shopping cart"() {

        when:
        cart.addProducts("p1", 10, 50)
        cart.addProducts("p2", 100, 5)

        then:
        cart.deleteProducts("p1", 1)
        !cart.deleteProducts("p1", -2)
        !cart.deleteProducts("p1", 50)
        cart.deleteProducts("p2", 5)
        !cart.deleteProducts("p3", 1)
    }

}
