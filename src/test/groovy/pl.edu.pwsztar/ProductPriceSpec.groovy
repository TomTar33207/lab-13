package pl.edu.pwsztar

import spock.lang.Specification

class ProductPriceSpec extends Specification {

    static ShoppingCart cart;

    def setupSpec() {
        cart = new ShoppingCart();
    }

    def "should get price of a product from shopping cart"() {

        when:
        cart.addProducts("p1", 1, 2)

        then:
        cart.getProductPrice("p1") == 1
        cart.deleteProducts("p1", 1)
        cart.getProductPrice("p1") == 1
        cart.addProducts("p1", 1, 10)
        cart.getProductPrice("p1") == 1
        cart.deleteProducts("p1", 11)
        cart.getProductPrice("p1") == 0
    }
}
