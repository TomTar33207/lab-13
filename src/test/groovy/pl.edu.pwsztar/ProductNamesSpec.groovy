package pl.edu.pwsztar

import spock.lang.Specification

class ProductNamesSpec extends Specification {

    static ShoppingCart cart;

    def setupSpec() {
        cart = new ShoppingCart();
    }

    def "should get names of products from shopping cart"() {

        when:
        cart.addProducts("p1", 1, 1)
        cart.addProducts("p2", 1, 1)
        cart.addProducts("p3", 1, 1)
        cart.addProducts("p4", 1, 1)

        then:
        cart.getProductsNames() == ["p1", "p2", "p3", "p4"]
        cart.deleteProducts("p1", 1)
        cart.getProductsNames() == ["p2", "p3", "p4"]
        cart.addProducts("p5", 1, 1)
        cart.getProductsNames() == ["p2", "p3", "p4", "p5"]
    }

}
